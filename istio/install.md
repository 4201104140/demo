istioctl install --set profile=demo
kubectl -n istio-system get deploy

istioctl install -f config.yaml
istioctl profile list
istioctl profile diff default demo

kubectl label namespace default istio-injection=enabled

istioctl install -f config.yaml --set meshConfig.outboundTrafficPolicy.mode=REGISTRY_ONLY

kubectl exec "$SOURCE_POD" -c sleep -- curl -sI https://www.google.com | grep  "HTTP/"; kubectl exec "$SOURCE_POD" -c sleep -- curl -sI https://edition.cnn.com | grep "HTTP/"

sudo kubectl apply -f httpbin.yaml

sudo kubectl exec "$SOURCE_POD" -c sleep -- curl -sS http://httpbin.org/headers
kubectl logs "$SOURCE_POD" -c istio-proxy | tail

sudo kubectl apply -f google.yaml
kubectl exec "$SOURCE_POD" -c sleep -- curl -sSI https://www.google.com | grep  "HTTP/"
kubectl logs "$SOURCE_POD" -c istio-proxy | tail

kubectl exec "$SOURCE_POD" -c sleep -- time curl -o /dev/null -sS -w "%{http_code}\n" http://httpbin.org/delay/5

kubectl delete serviceentry httpbin-ext google
kubectl delete virtualservice httpbin-ext --ignore-not-found=true