kubectl apply -f samples/sleep/sleep.yaml
export SOURCE_POD=$(kubectl get pod -l app=sleep -o jsonpath={.items..metadata.name})

kubectl apply -f samples/httpbin/httpbin.yaml

kubectl exec "$SOURCE_POD" -c sleep -- curl -sS -v
kubectl exec "$SOURCE_POD" -c sleep -- curl -sS -v httpbin:8000/status/418
kubectl logs -l app=sleep -c istio-proxy