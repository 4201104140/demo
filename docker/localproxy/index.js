const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

// Endpoint to receive data and store it in local storage
app.post('/storeData', (req, res) => {
    const { key, value } = req.body;
    localStorage.setItem(key, value);
    res.sendStatus(200);
});

app.listen(3000, () => {
    console.log('Proxy service running on port 3000');
});