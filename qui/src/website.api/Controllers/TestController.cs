﻿using Microsoft.AspNetCore.Mvc;

namespace website.api.Controllers
{
    [ApiController]
    [Route("api/v1/webcms")]
    public class TestController : ControllerBase
    {
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok();
        }
    }
}
