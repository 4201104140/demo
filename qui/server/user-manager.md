New-LocalGroup -Name "Docker"

New-LocalUser -Name "QuiUpload" -Group "Docker" -Password (ConvertTo-SecureString "1qazZAQ!" -AsPlainText -Force)
New-LocalUser -Name "QuiUpload" -Group "Docker" -Password (ConvertTo-SecureString "1qazZAQ!" -AsPlainText -Force)

New-LocalUser -Name "QuiUpload" -Password (ConvertTo-SecureString "1qazZAQ!" -AsPlainText -Force)
New-LocalUser -Name "ContainerUser" -Password (ConvertTo-SecureString "1qazZAQ!" -AsPlainText -Force)
Add-LocalGroupMember -Group "Docker" -Member "QuiUpload"
Add-LocalGroupMember -Group "Docker" -Member "ContainerUser"
Get-LocalGroupMember -Group "Docker"
icacls "C:\Path\To\Folder" /grant Qui:(OI)(CI)RX

icacls "C:\Path\To\Folder" /grant Qui:(OI)(CI)F

docker-compose exec api-upload powershell -Command "Get-WmiObject Win32_UserAccount | Select-Object Name, SID"

docker-compose exec api-upload cmd /c "net user"