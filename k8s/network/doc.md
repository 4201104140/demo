sudo kubectl create -n istio-system secret tls b-com \
  --key=cert/b.key \
  --cert=cert/b.crt