ISTIOD=$(sudo kubectl get -A pods --selector=app=istiod -o jsonpath='{.items[*].metadata.name}')

sudo kubectl exec -it $ISTIOD -n istio-system -- curl external-svc-redirect | jq '.[] | .hostname'

sudo kubectl exec -it $ISTIOD -n istio-system -- curl localhost:8080/debug/registryz | jq '.[] | .hostname'

sudo kubectl exec -it $ISTIOD -n istio-system -- curl localhost:8080/debug/instancesz | jq 'keys'

sudo kubectl apply -f gateway.yaml
sudo kubectl delete -f gateway.yaml

https://istio.io/latest/docs/ops/common-problems/network-issues/#gateway-mismatch
https://www.learncloudnative.com/blog/2023-02-10-servicentry-feature

iptables -t nat -nL KUBE-SERVICES | grep ingressgateway


kubectl apply -f samples/sleep/sleep.yaml

kubectl delete -f samples/sleep/sleep.yaml

export SOURCE_POD=$(kubectl get pod -l app=sleep -o jsonpath='{.items..metadata.name}')

kubectl exec "$SOURCE_POD" -c sleep -- curl -sSI https://www.google.com | grep  "HTTP/"; kubectl exec "$SOURCE_POD" -c sleep -- curl -sI https://edition.cnn.com | grep "HTTP/"

kubectl exec "$SOURCE_POD" -c sleep -- curl -sSI https://a.com | grep  "HTTP/"; kubectl exec "$SOURCE_POD" -c sleep -- curl -sI https://edition.cnn.com | grep "HTTP/"

kubectl logs "$SOURCE_POD" -c istio-proxy | tail

kubectl exec "$SOURCE_POD" -c sleep -- curl -sS http://httpbin.org/headers

kubectl exec "$SOURCE_POD" -c sleep -- curl -sS http://a.com

kubectl exec "$SOURCE_POD" -c sleep -- curl -sS https://a.com

kubectl exec "$SOURCE_POD" -c sleep -- ping a.com