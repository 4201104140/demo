const proxyUrl = 'http://lsp.azitest.com';

// Function to save data to local storage via proxy
function saveDataToLocalStorage(key, value) {
    fetch(`http://lsp.azitest.com/storeData`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ key, value }),
    });
}