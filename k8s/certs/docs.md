Create the root key

openssl ecparam -out azicloud.key -name prime256v1 -genkey

Create a Root Certificate and self-sign it

openssl req -new -sha256 -key azicloud.key -out azicloud.csr

Use the following command to generate the Root Certificate.
openssl x509 -req -sha256 -days 365 -in azicloud.csr -signkey azicloud.key -out azicloud.crt


openssl ecparam -out cowk8s.key -name prime256v1 -genkey
openssl req -new -sha256 -key cowk8s.key -out cowk8s.csr
openssl x509 -req -in cowk8s.csr -CA  azicloud.crt -CAkey azicloud.key -CAcreateserial -out cowk8s.crt -days 365 -sha256
openssl x509 -in cowk8s.crt -text -noout

openssl ecparam -out a.key -name prime256v1 -genkey
openssl req -new -sha256 -key a.key -out a.csr
openssl x509 -req -in a.csr -CA  azicloud.crt -CAkey azicloud.key -CAcreateserial -out a.crt -days 365 -sha256
openssl x509 -in a.crt -text -noout


openssl ecparam -out b.key -name prime256v1 -genkey
openssl req -new -sha256 -key b.key -out b.csr
openssl x509 -req -in b.csr -CA  azicloud.crt -CAkey azicloud.key -CAcreateserial -out b.crt -days 365 -sha256
openssl x509 -in b.crt -text -noout